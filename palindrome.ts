// Ecrire une fonction qui renvoie true si un nombre est un palindrome, false sinon.

// Exemples :
// 121 est un palindrome
// 123 n'est pas un palindrome
// -121 n'est pas un palindrome

const isPalindrome = (x: number): boolean => {
  return false;
};

console.log(isPalindrome(121));
console.log(isPalindrome(123));
console.log(isPalindrome(-121));
