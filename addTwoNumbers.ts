// Input: 2 listes chainées
// Chaque node contient un nombre entier et un pointeur vers le suivant
// les digites sont chainée dans l'ordre inversé => [7, 0, 3] = 307
// Output: la somme des deux listes

// Exemples :
// [2, 4, 3] + [5, 6, 4] = 342 + 465 = 807 = [7, 0, 8]

class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }

  static fromArray(arr: number[]): ListNode | null {
    if (arr.length === 0) {
      return null;
    }
    return new ListNode(arr.shift(), this.fromArray(arr));
  }

  toArray(): number[] {
    const arr: number[] = [];
    let currentNode: ListNode | null = this;
    while (currentNode !== null) {
      arr.push(currentNode.val);
      currentNode = currentNode.next;
    }
    return arr;
  }
}

const addTwoNumbers = (
  l1: ListNode | null,
  l2: ListNode | null
): ListNode | null => {
  return null;
};

console.log(ListNode.fromArray([2, 4, 3]));
console.log(ListNode.fromArray([5, 6, 4])?.toArray());
