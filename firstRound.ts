// Trier input1 dans l'ordre croissant. afficher le tableau.
// Trier input2 dans l'ordre croissant des dates de naissance. afficher le tableau.

const input1 = [4, 5, 3, 345, 432, 23, -1, 4];

const input2 = [
  {
    name: "Michel",

    dateOfBirth: "1990-04-23",
  },

  {
    name: "Bernard",

    dateOfBirth: "1991-07-12",
  },

  {
    name: "Bertrand",

    dateOfBirth: "1990-09-09",
  },
];

// ton code ici:

//

// On te donne 2 fonction async qui retourne un nombre. Ecrie une fonction qui renvoie la somme des nombres.
const getNumber1 = async (): Promise<number> => {
  return 4;
};

const getNumber2 = async (): Promise<number> => {
  return 12;
};

// ton code ici

//

// Ecrire une fonction qui donne les N premier nombre de la suite de fibonacci.

// Exemple : N = 5
// [0, 1, 1, 2, 3]

// Ton code ici

//
