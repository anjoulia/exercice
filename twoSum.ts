// Un array de nombre est donné et un nombre entier cible.
// Retourne dans un array les deux index des nombres dont la somme est égal au nombre entier cible.

// Exemple :
// [2, 7, 11, 15] et cible = 13
// retourne [0, 2]

// [3, 2, 4] et cible = 6
// retourne [1, 2]

// [3, 3] et cible = 6
// retourne [0, 1]
const towSum = (nums: number[], target: number): number[] => {
  return [];
};

console.log(towSum([2, 7, 11, 15], 9));
console.log(towSum([3, 2, 4], 6));
console.log(towSum([3, 3], 6));
